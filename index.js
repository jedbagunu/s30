//1. Count the total stocks
db.fruits.aggregate(
  [
    {
      $match: {
        stock: {
          $gte: 0
        }
      }
    },
    {
      $count: "enoughStocks"
    }
  ]
)
//2. count stock morethan 20
db.fruits.aggregate(
  [
    {
      $match: {
        stock: {
          $gte: 15
        }
      }
    },
    {
      $count: "fruits on sale"
    }
  ]
)
//3. avg price per supplier
db.fruits.aggregate([
	{
	 $match: { supplier_id: 1 } 
	},
        {
        $group:  {
			_id: "$supplier_id",
			avgPrice:{$avg:"$price"}
		}
            
        }
        
	
])


        

//4. avg price
db.fruits.aggregate([
    {
        $match: { onSale: true }
    },
    {
        $group: {
            _id: "$supplier_id",
            avg_price: { $avg: "$price" }
        }
    }
])

//5. max price
db.fruits.aggregate([
    {
        $match: { onSale: true }
    },
    {
        $group: {
            _id: "$supplier_id",
            max_price: { $max: "$price" }
        }
    }
])


//6.min price

db.fruits.aggregate([
    {
        $match: { onSale: true }
    },
    {
        $group: {
            _id: "$supplier_id",
            min_price: { $min: "$price" }
        }
    }
])

